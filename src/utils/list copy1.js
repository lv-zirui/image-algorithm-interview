export const problemLists = [
  {
    id: 1,
    uid: 1,
    name: "闭包是什么? 有什么缺点?",
    centent:
      "闭包的含义就是: 一个作用域有权访问另一个作用域的局部变量, 缺点: 使用不当可能会造成内存的泄露",
  },
  {
    id: 2,
    uid: 2,
    name: "TS和JS有什么区别?",
    centent:
      "TS其实就是JS的超集, 在JS中我们定义一个变量是字符串类型, 但是我们可以更改该变量为数值类型, 所以, 在开发中, 类型可以随意更改, 会造成bug的出现, 延长了找bug的时间, 但是TS加强了JS的类型, 配合vsCode可以很快的找到哪里出现的问题",
  },
  {
    id: 3,
    uid: 1,
    name: "请你简单说一下Vue2的响应式原理",
    centent:
      "其实就是数据劫持+发布订阅, 当把一个普通javascript对象传给Vue实例来作为它的data选项时, Vue将遍历它的属性, 用Object.defineProperty()监听他们的getter/setter方法, 这样, 他们就可以让Vue追踪依赖, 在对象的属性被访问(get)和修改(set)时通知变化",
  },
  {
    id: 4,
    uid: 2,
    name: "什么是postcss, 以及postcss有什么作用?",
    centent:
      "1.首先明确 postcss 是一个平台, 2.基于这个平台, 可以使用一些插件, 来优化 css 的代码, 比如说: autoprefixer 插件, 他就需要基于 postcss 使用, 作用是可以帮助我们为css增加上不同的浏览器前缀。",
  },
  {
    id: 5,
    uid: 3,
    name: "为什么要初始化css样式",
    centent:
      "因为浏览器的兼容问题, 不同浏览器对有些元素的默认样式是不同的, 并且默认的样式会因修改那个到项目最终的布局, 如果没对css初始化, 那么有可能会出现浏览器之间的页面显示差异",
  },
  {
    id: 6,
    uid: 2,
    name: "请描述一下 cookies, sessionStorage, localStorage 的区别?",
    centent:
      "cookies一般情况下是用来标记用户身份的一段数据, 一般是一段加密的字符串, 并且在默认的情况下, 我们的cookies只会在同源的 HTTP 请求中携带, sessionStorage呢其实是浏览器本地存储的一种方式, 并且是以键值对的形式存储到我们的浏览器本地, 但是我们通过sessionStorage存储的数据, 会在浏览器关闭之后自动删除, localStorage正好弥补了sessionStorage的不足, localStorage它也是浏览器本地存储的一种方式, 同样它也以键值对的形式存在, 不过呢, 我们通过localStorage保存的数据是一个持久化的数据, 一般情况下来说, 如果我们不主动去删除的情况下, 我们保存的数据会一直存在于我们的一个本地",
  },
  {
    id: 7,
    uid: 2,
    name: "请说出display: none;和visibility: hidden;的区别",
    centent:
      "他们都会将元素隐藏(不可见), 但是不同的地方就在于, display: none; 设置之后, 元素会从我们的渲染树中消失, 不再占据任何的空间, 然而我们设置visibility为hidden时, 并不会将元素从渲染树上消失, 只是将内容进行隐藏, 依然占用空间,   display: none; 是非继承属性, 子孙节点消失是因为元素从渲染树消失造成, 通过修改子孙节点属性无法显示; 而visibility: hidden; 是继承属性, 子孙节点消失由于继承了hidden, 通过设置visibility: visible; 可以让子孙节点显示",
  },
  {
    id: 8,
    uid: 1,
    name: "什么是同步? 什么是异步? 他们的区别时什么?",
    centent:
      "比如说: 我给移动打电话, 让他帮忙查一下花费, 他说稍等, 我们不会立马挂断电话, 一会他说: 你的花费还有11, 这个时候, 我们才会挂断电话, 这个操作就是同步, 异步呢, 比如说: 朋友打电话找我借钱, 立马就挂断了, 我把钱转给他之后, 通知了他一下, 说转过去了, 这个过程呢,其实就是异步",
  },
  {
    id: 9,
    uid: 3,
    name: "<keep-alive></keep-alive>的作用是什么?",
    centent:
      " keepAlive的作用: <keep-alive></ keep-alive>包裹动态组件, 会魂村不活动的组件实例, 主要用于保留组件状态或避免重新渲染",
  },
];