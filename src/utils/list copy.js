export const problemLists = [
  {
    id: 1,
    uid: 1,
    name: "1*1的卷积核有什么用",
    br: true,
    isMath: true,
    // math: "\\frac{-b \\pm \\sqrt{b^2-4ac}}{2a}",
    // math: "\\sqrt[2](63.6-(60a+b))^2+(65.2-(62a+b))^2+(66 − (64𝑎 + 𝑏))^2",
    // math: `$ y = ax^2 + bx + c $`,
    // math: `\\begin{matrix} 0 & 1 \\\\ 1 & 0 \\end{matrix}\\quad
    // \\begin{pmatrix} 0 & -i \\\\ i & 0 \\end{pmatrix}\\\\
    // \\begin{bmatrix} 0 & -1 \\\\ 1 & 0 \\end{bmatrix}\\quad
    // \\begin{Bmatrix} 1 & 0 \\\\ 0 & -1 \\end{Bmatrix}\\\\
    // \\begin{vmatrix} a & b \\\\ c & d \\end{vmatrix}\\quad
    // \\begin{Vmatrix} i & 0 \\\\ 0 & -i \\end{Vmatrix}`,
    math: "y = ax^2 + bx + c",
    // math: "{{}_{ }^{ } \int _{ }^{ }k \text{d} x=kx+C}",
    // math: "\frac{-b \pm \sqrt{b^2-4ac}}{2a}",
    // math: `𝐷_{𝑃_1 𝑃_2 }=max⁡(𝑑𝑖𝑠𝑡(𝑃_1,𝑃_2 ),𝑑𝑖𝑠𝑡(𝑃_2,𝑃_1))`,
    // math: "$RMS[\Delta x]_{t-1} = \sqrt{E[\Delta x^2]_{t-1} + \epsilon}$",
    centent: `<ul>
      <li>1. 1x1卷积，不会改变特征图的h, w</li>
      <li>2. 1x1卷积可以改变特征图的数量。大家都叫升维或者降维，但实际上不改变h、w，dim也不改变，只是改变了输出的channel，改变的是shape</li>
      <li>3. 如果1x1输出的channel变少时，参数量和计算量也在下降 </li>
      <li>4. 增加了非线性，增加了网络的表达能力</li>
      <li>5. 空间信息的沟通和融合（+）</li>
      <li> 6. 能够起到全连接的作用e</li>
      </ul> `,
  },
  {
    id: 2,
    uid: 2,
    name: "浅层的CNN和深层的CNN提取的特征有何区别",
    centent: "浅层网络得到的是几何信息。深层得到的是语义信息",
  },
  {
    id: 3,
    uid: 1,
    name: "卷积的参数量与输入图像的尺寸有关吗？",
    centent: "只与输入channel与输出channel，以及卷积核w、h有关",
  },
  {
    id: 4,
    uid: 2,
    name: "将几何信息与语义信息相结合，训练效果变好还是会变差",
    centent: "预测效果通常会比较好",
  },
  {
    id: 5,
    uid: 3,
    name: "请简单介绍下高通滤波和低通滤波",
    centent:
      "拉普拉斯滤波，sobel滤波，scharr滤波是高通滤波，低通滤波包含均值滤波，中值滤波，双边滤波，方框滤波",
  },
  {
    id: 6,
    uid: 2,
    name: "小目标过多时，该如何处理",
    centent:
      "CNN的特征提取好坏受目标的大小的影响。经过CNN层要保留更好的特征，当小目标过多时，可能需要裁减一些CNN层",
  },
  {
    id: 7,
    uid: 2,
    name: "为什么说2个3x3=5x5?",
    centent: "用3x3的卷积来堆叠，感受野是一样的，而且参数量会减少",
  },
  {
    id: 8,
    uid: 1,
    name: "如果修改了网络的输入尺寸,会影响cnn的参数量吗",
    centent: "不会影响CNN的参数量，但是会影响全连接的参数量",
  },
  {
    id: 9,
    uid: 3,
    name: "图像金字塔有哪几种，简述原理",
    centent:
      "包含高斯金字塔和拉普拉斯金字塔，高斯金字塔分为向上采样放大和向下采样缩小，拉普拉斯金字塔是经过低通滤波，缩小尺寸，放大尺寸，图像相减得到的",
  },
];
